<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">
<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>

<body <?php print $body_classes; ?>>
  <?php if (!empty($admin)) print $admin; ?>
  <div id="page-wrapper" class="full-width">
  <div id="page" <?php print $page_classes; ?>>

   <div id="page-inner" class="inner">
      <div id="header-wrapper" class="full-width">
          <div id="header" <?php print $header_classes; ?>>
            <div id="header-inner" class="inner">
                <?php print $top; ?>
                <?php print $logo_title; ?>
                <?php print $logo; ?>
                <?php print $site_name; ?>
                <?php print $site_slogan; ?>
                <?php print $search_box; ?>
                <?php print $mission; ?>
                <?php print $header_top; ?>
                    <?php print $primary_links; ?>
                    <?php print $secondary_links; ?>
                <?php print $header_content; ?>
             </div> <!-- /header-inner -->
            </div> <!-- /header -->
      </div> <!-- /header-wrapper -->
   <div id="container-wrapper" class="full-width">
    <div id="container" <?php print $container_classes; ?>>
     <div id="container-inner" class="inner">
     <?php print $left; ?>
     <div id="main" <?php print $main_classes; ?>>
      <div id="main-inner" class="inner">
          <?php print $page_title; ?>
          <?php print $feed_icons; ?>
          <?php print $preface_top; ?>
          <?php print $preface_bottom; ?>
          <?php print $tabs; ?>
          <?php print $messages; ?>
          <?php print $help; ?>
          <?php print $content_top; ?>
          <?php print $content; ?>
          <?php print $content_bottom; ?>
          <?php print $postscript_top; ?>
          <?php print $postscript_bottom; ?>
          <?php print $breadcrumb; ?>
       </div> <!-- /main-inner -->
      </div> <!-- /main -->
      <?php print $right; ?>
     </div> <!-- /container-inner -->
    </div> <!-- /container -->
   </div> <!-- /container-wrapper -->

     <div id="footer-wrapper" class="full-width">
      <div id="footer" <?php print $footer_classes; ?>>
       <div id="footer-inner" class="inner">
        <?php print $footer_message; ?>
        <?php print $footer_content; ?>
        <?php print $bottom; ?>
       </div> <!-- /footer-inner -->
      </div> <!-- /footer -->
     </div> <!-- /footer-wrapper -->
     <?php print $closure; ?>

   </div> <!-- /page-inner -->
  </div> <!-- /page -->
</div> <!-- /page-wrapper -->

</body>
</html>